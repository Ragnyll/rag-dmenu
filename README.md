# Ragnyll's st build #
Forked from https://git.suckless.org/dmenu version 4.9

- build with `sudo make && sudo make install`

Patches applied with `patch --merge -i (patch_name)`

To uninstall remove entries from `/usr/local` and `$PREFIX/share/man`

## patches applied (in the order applied):##
- [border](https://tools.suckless.org/dmenu/patches/border/dmenu-border-4.9.diff)
- [center](https://tools.suckless.org/dmenu/patches/center/dmenu-center-20200111-8cd37e1.diff)
- [highlight](https://tools.suckless.org/dmenu/patches/highlight/dmenu-highlight-4.9.diff)
- [lineheight](https://tools.suckless.org/dmenu/patches/line-height/dmenu-lineheight-4.9.diff)
- [numbers](https://tools.suckless.org/dmenu/patches/numbers/dmenu-numbers-4.9.diff)
- [fuzzymatch](https://tools.suckless.org/dmenu/patches/fuzzymatch/dmenu-fuzzymatch-4.9.diff)
- [rejectnomatch](https://tools.suckless.org/dmenu/patches/reject-no-match/dmenu-rejectnomatch-4.7.diff)
